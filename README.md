# Test python data eng

Le compte rendu du test - Mohammad Baraa AL BAKRI

## 0 - Trouver les résultats des questions 3 et 4

-----------
Pour trouver les résultats des questions 3 et 4 sans lancer le code en local, il suffit d'ouvrir le dernier 
gitlab CI pipeline, puis, lire le dernier log du job `run` pour retrouver la réponse de la question 4, 
et la réponse de la question 3 se trouve dans le fichier artifact `output/step2.json` 

## 1- L'arborescence des fichiers


-----------
    ├── README.md
    ├── archive
    ├── data
    │ ├── clinical_trials.csv
    │ ├── drugs.csv
    │ ├── pubmed.csv
    │ └── pubmed.json
    ├── main.py
    ├── makefile
    ├── output
    │ ├── step1.json
    │ └── step2.json
    ├── requirements.txt
    ├── scripts
    │ ├── __init__.py
    │ ├── analyze
    │ │ ├── __init__.py
    │ │ ├── journal_analyze.py
    │ │ └── journal_with_most_mentions.py
    │ ├── common
    │ │ ├── __init__.py
    │ │ └── utils.py
    │ └── pipeline
    │     ├── __init__.py
    │     ├── configs.py
    │     ├── data_pipeline.py
    │     ├── data_tools.py
    │     ├── step1.py
    │     └── step2.py
    ├── setup.py
    ├── tests
    │ ├── __init__.py
    │ └── common
    │     ├── __init__.py
    │     └── test_common.py
    └── tmp

Le point d'entrée est le fichier `main.py`


## 2- Data pipeline 

-----------
### Choix d'implémentation
J'ai choisi de mettre en place un data pipeline qui contient deux étapes indépendantes,
ce qui nous permet de mettre le code de chaque étape dans une tâche séparée. 

### Les différentes étapes du data pipeline
Le Data pipeline est divisé en deux étapes:
1. Step 1 : Cette étape permet de lire les deux fichiers d'entrées:
- `drugs.csv`
- `pubmed.csv`

    Puis, chercher les liaisons entre les différents médicaments et leurs mentions respectives dans 
    les différentes publications PubMed, et à la fin, stocker le résultat dans le fichier:
    `output/step1.json`

2. Step 2 : Cette étape permet de lire le fichier d'entrée:
- `clinical_trials.csv`

    Ensuite, lire le fichier de sortie de l'étape 1, puis chercher les liaisons entre les différents 
    médicaments et leurs mentions respectives dans les différentes publications scientifiques, et à la fin,
    stocker le résultat dans le fichier:
   `output/step2.json`

Le résultat du data pipeline se trouve donc dans le fichier `output/step2.json`

### Comment exécuter le script
Pour exécuter le script en local :
- Installer Python 3
- Lancer les commandes :
`make init`
`make install`
`make run`
en ordre.
- Le script lancera d'abord le data pipeline, puis, la fonction pour extraire le nom du journal
qui mentionne le plus de médicaments différents.

### Comment le data pipeline fonctionne
1. La step 1 commence l'exécution par déplacer les fichiers d'entrées et les mettre dans
le repo temporaire
2. Puis, elle lit le contenu des fichiers `drugs.csv` et `pubmed.csv`.
3. Après, elle cherche les publications pubmed qui mentionnent chaque drug
et elle les rajoute dans l'objet drug + elle rajoute séparément le nom du journal dans la
liste des journaux dans l'objet drug.
4. A la fin de la step 1, le résultat est écrit dans le fichier `output/step1.json`
et les deux fichiers d'entrées sont déplacés au repo d'archive.
5. Puis, la step 2 commence et elle déplace le fichier d'entrée `clinical_trials.csv` dans
le repo temporaire.
6. Puis, elle lit le fichier.
7. La step 2 lit le fichier de sortie de la step 1 `output/step1.json`
8. Ensuite, elle cherche les publications scientifiques qui mentionnent chaque drug 
et elle les rajoute dans l'objet drug + elle rajoute séparément le nom du journal dans la
liste des journaux dans l'objet drug.
9. A la fin de la step 2, le résultat est écrit dans le fichier `output/step2.json`
et le fichier d'entrée est déplacé au repo d'archive.


## Extraire le nom du journal
Le script `scripts/analyze/journal_with_most_mentions.py` permet d'extraire le nom du journal
qui mentionne le plus de médicaments différents.


## Comment faire évoluer le code pour gérer de grosses volumétries
Voici mes propositions :
1. Séparer les étapes de lecture de données et les étapes de regroupement des données et utiliser des tables partitionnés:
    Afin de mieux gérer les données d'entrées, j'aurais préféré créer des jobs qui
    lisent les fichiers d'entrées, puis stocker leurs contenus dans un système de stockage
    structuré (exemple : GCP Bigquery), chaque type de données (drugs, publication pubmed, publication clinical_trials)
    pourra être stocké dans une table partitionnée, puis en utilisant un job à part, nous regrouperons
    les nouvelles données ensemble et nous stockerons le résultat dans une 4ᵉ table partitionnée.
2. Utiliser Pyspark pour traiter les données en parallèle :
    Pyspark permet de traiter une grande quantité de données en parallèle, puis nous pourrons lancer le job
    sur un cluster HDP en local ou sur le cloud (exemple: GCP Dataproc).
3. Utiliser des outils cloud managés pour faciliter le traitement, exemples : bigquery, dataflow, kubernetes.


## Réponses aux questions SQL
1.  La requête SQL pour retrouver le chiffre d’affaires par date
```
    SELECT
        date,
        sum(prod_price * prod_qty) AS ventes
    FROM
        TRANSACTIONS
    where
        date between "2019-01-01"
        and "2019-12-31"
    GROUP BY
        date
    order by
        date;
```

2. La requête SQL pour retrouver les vents par client.

```
    SELECT
        T.client_id,
        sum(
            IF(
                P.product_type = "MEUBLE",
                T.prod_price * T.prod_qty,
                0
            )
        ) AS ventes_meuble,
        sum(
            IF(
                P.product_type = "DECO",
                T.prod_price * T.prod_qty,
                0
            )
        ) AS ventes_deco
    FROM
        TRANSACTIONS AS T
        inner join PRODUCT_NOMENCLATURE AS P ON T.prod_id = P.product_id
    where
        date between "2019-01-01"
        and "2019-12-31"
    GROUP BY
        T.client_id;
```

Ce n'est pas spécifié dans le sujet si vous souhaitiez que j'ajoute des tests ou pas, n'hésiter pas à m'informer
si vous souhaiter avoir des tests unitaires ou d'intégrations.
