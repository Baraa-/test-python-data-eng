from unittest import TestCase
from scripts.common.utils import get_file_name_from_path


class UtilsTests(TestCase):

    @staticmethod
    def test_get_file_name_from_path_return_file_name():
        # When
        file_path = "toto/titi/filename.txt"

        # Then
        assert 'filename.txt' == get_file_name_from_path(file_path)
