import os

from setuptools import setup, find_packages

tests_require = [
    'pytest==6.2.5',
    'pytest-cov==2.12.1'
]


setup(name='data-pipeline',
      version='1.0',
      description='Tool to run a data pipeline on Servier inout files',
      author='Mohammad Baraa AL BAKRI',
      packages=find_packages(exclude=['tests', 'tests.*', '*.test.*']),
      tests_require=tests_require,
      install_requires=[
      ],
      extras_require={
          'tests': tests_require
      },
      )
