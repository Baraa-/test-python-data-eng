PY=python3
SHELL := /bin/bash
PYTHON3_OK := $(shell python3 --version 2> /dev/null)

.PHONY:
	init
	install
	test
	lint
	run

.DEFAULT: help
help:
	@echo "make init"
	@echo "       prepare development environment, use only once"
	@echo "make install"
	@echo "       install development requirements"
	@echo "make test"
	@echo "       run tests"
	@echo "make lint"
	@echo "       run pylint"
	@echo "make run"
	@echo "       run main file"

init: check-python
	python3 -V
	python3 -m venv .env
	source .env/bin/activate
install:
	pip3 install -r requirements.txt
	python setup.py install
test:
	pytest .
lint:
	pylint scripts
run:
	python main.py

check-python:
ifndef PYTHON3_OK
	$(error package 'python3' not found)
endif
