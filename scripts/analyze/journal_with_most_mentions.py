"""
Entry point of journal analyzes
"""
import logging

from scripts.pipeline.configs import STEP2_CONFIGS
from scripts.analyze.journal_analyze import get_journal_with_max_mentions


def get_journal_with_most_mentions():
    """
    Get the name of the journal that mention the most drugs
    """
    logging.getLogger().setLevel(logging.INFO)
    logging.info('Getting the name of the journal that mention the most drugs.')
    journal_name = get_journal_with_max_mentions(STEP2_CONFIGS['STEP2_OUTPUT_PATH'])
    logging.info(f'The journal that mention the most drugs is: "{journal_name}".')
