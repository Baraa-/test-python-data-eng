"""
Journal analyzing tools
"""
import re

from scripts.common.utils import read_json_file


def clean_journal_list(journal_list):
    """
    Remove special characters from journal's name
    :param journal_list: The list of journal names
    :return: The cleaned list
    """
    return [re.findall(r'([\w\d ]*)', journal)[0].lower() for journal in journal_list]


def get_journal_with_max_mentions(drugs_with_mentions_file_path):
    """
    Find the journal with most drugs mentions
    :param drugs_with_mentions_file_path: The output file of data pipeline
    :return: The name of the journal with most drugs mentions
    """
    drugs = read_json_file(drugs_with_mentions_file_path)
    journal_list = []
    for drug in drugs:
        journal_mentions = drug['journal_mentions']
        if len(journal_mentions) == 0:
            continue

        journal_names = [journal['journal'] for journal in journal_mentions]
        journal_list.append(journal_names)

    # Get flatten list
    flatten_journal_list = sum(journal_list, [])
    if len(flatten_journal_list) == 0:
        raise Exception('Didn\'t find any journal in drugs file.')
    cleaned_journal_list = clean_journal_list(flatten_journal_list)

    most_frequent_journal = max(set(cleaned_journal_list), key=cleaned_journal_list.count)
    return most_frequent_journal
