"""
Tools used by Data pipeline's steps
"""

import sys
import logging

from scripts.common.utils import move_file, read_csv_file
from scripts.pipeline.configs import TMP_FOLDER_PATH, ARCHIVE_FOLDER_PATH, SUCCESS_STATUS, FAILED_STATUS


def add_mention_to_drug_if_found(drug, mention, mention_column_name, publication_mentions_key, journal_mentions_key):
    """
    Verify if the drug is included in publication mention, if so, add the information of the mention in the drug
    :param drug: Drug object
    :param mention: Mention object
    :param mention_column_name: The name of the title column(key) of the mention
    :param publication_mentions_key: The key of the list in which the mention information will be added in drug object
    :param journal_mentions_key:  The key of the list in which the journal that published the publication will be added
    """
    if drug['drug'].lower() in mention[mention_column_name].lower():
        drug[publication_mentions_key].append(
            {
                'id': mention['id'],
                'title': mention[mention_column_name],
                'date': mention['date']
            }
        )
        drug[journal_mentions_key].append(
            {
                'journal': mention['journal'],
                'date': mention['date']
            }
        )


def read_input_file(file_path: str, file_format: str = 'csv') -> tuple:
    """
    Move the input file to the temporary folder, then read it, and return its content
    :param file_path: The path of the input file
    :param file_format: The format of the input file
    :return: Input file's content
    """
    file_format_lower = file_format.lower()
    if file_format_lower == 'csv':
        tmp_file_path = move_file(file_path, TMP_FOLDER_PATH)
        data = read_csv_file(tmp_file_path)
    else:
        raise Exception(f'Unsupported file format: "{file_format}"')
    return data, tmp_file_path


def archive_file(file_path: str):
    """
    Move a file to the Archive folder
    :param file_path: The path of the file
    """
    logging.info(f'Moving file: "{file_path}" to archive folder: "{ARCHIVE_FOLDER_PATH}".')
    move_file(file_path, ARCHIVE_FOLDER_PATH)
    logging.info('File is moved to archive.')


# pylint: disable=broad-except
def run_ingestion_job(job_name: str, ingestion_function, *args) -> str:
    """
    Run an ingestion function in safe mode to catch exceptions
    :param job_name: The name of the job to run
    :param ingestion_function: The function that will be used to run the ingestion
    :param args: The args of the ingestion_function
    :return: Job status
    """
    try:
        ingestion_function(*args)
    except Exception as e_message:
        logging.error(f'{job_name} failed with exception: "{e_message}"')
        return FAILED_STATUS
    return SUCCESS_STATUS


def check_job_status(job_status: str):
    """
    Check the status of the job and stop pipeline's execution if failed
    :param job_status: Job status
    """
    if job_status == FAILED_STATUS:
        logging.error('Job failed -> Stop data pipeline execution.')
        sys.exit()
