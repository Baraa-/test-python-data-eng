"""
Data pipeline's step1 entry point
"""
import logging

from scripts.common.utils import data_list_to_json_file
from scripts.pipeline.data_tools import read_input_file, run_ingestion_job, archive_file, add_mention_to_drug_if_found


def _get_drugs_input_data(drugs_file_path: str) -> tuple:
    """
    Read and get the list of drugs
    :param drugs_file_path: The input file in which the drugs exist
    :return: the drugs list + the new path of the drugs file in the temporary folder
    """
    logging.info('Getting drugs list.')
    drugs, tmp_file_path = read_input_file(drugs_file_path)
    logging.info(f'Found {len(drugs)} drugs in the file.')
    return drugs, tmp_file_path


def _find_drugs_pubmed_publication_mentions(step1_configs: dict):
    """
    Find pubmed publication mentions for each drug
    :param step1_configs: The configs of the step1
    """
    drugs, drugs_tmp_file_path = _get_drugs_input_data(step1_configs.get('DRUGS_FILE_PATH'))

    logging.info('Reading pubmed file to get publication mentions.')
    mentions, tmp_file_path = read_input_file(step1_configs.get('PUBMED_FILE_PATH'))
    logging.info(f'Found {len(mentions)} pubmed publication mentions.')

    logging.info('Finding drugs pubmed publication mentions...')
    for drug in drugs:
        drug['pubmed_publication_mentions'] = []
        drug['journal_mentions'] = []
        for mention in mentions:
            add_mention_to_drug_if_found(drug, mention, step1_configs.get('PUBMED_COLUMN_MENTION_NAME'),
                                         'pubmed_publication_mentions', 'journal_mentions')

    logging.info('Writing result in output file...')
    data_list_to_json_file(drugs, step1_configs.get('STEP1_OUTPUT_PATH'))

    archive_file(drugs_tmp_file_path)
    archive_file(tmp_file_path)


def step1_job(step1_configs: dict) -> str:
    """
    Run step1 job
    :param step1_configs: The configs of the step1
    :return: The status of the job
    """
    return run_ingestion_job('step1_job', _find_drugs_pubmed_publication_mentions, step1_configs)
