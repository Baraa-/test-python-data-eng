"""
Data pipeline Configs
"""

# Folders
INPUT_FOLDER_PATH = 'data'
TMP_FOLDER_PATH = 'tmp'
ARCHIVE_FOLDER_PATH = 'archive'
OUTPUT_FOLDER_PATH = 'output'


# Step 1 configs
STEP1_CONFIGS = {
    'DRUGS_FILE_PATH': f'{INPUT_FOLDER_PATH}/drugs.csv',
    'PUBMED_FILE_PATH': f'{INPUT_FOLDER_PATH}/pubmed.csv',
    'PUBMED_COLUMN_MENTION_NAME': 'title',
    'STEP1_OUTPUT_PATH': f'{OUTPUT_FOLDER_PATH}/step1.json'
}

# Step 2 configs
STEP2_CONFIGS = {
    'CLINICAL_TRIALS_FILE_PATH': f'{INPUT_FOLDER_PATH}/clinical_trials.csv',
    'CLINICAL_COLUMN_MENTION_NAME': 'scientific_title',
    'STEP2_OUTPUT_PATH': f'{OUTPUT_FOLDER_PATH}/step2.json'
}

# Job status
SUCCESS_STATUS = 'success'
FAILED_STATUS = 'failed'
