"""
Data pipeline entry point
"""

import logging

from scripts.pipeline.configs import STEP1_CONFIGS, STEP2_CONFIGS
from scripts.pipeline.data_tools import check_job_status
from scripts.pipeline.step1 import step1_job
from scripts.pipeline.step2 import step2_job


def run_data_pipeline():
    """
    Main function to trigger the data pipeline
    """
    logging.getLogger().setLevel(logging.INFO)

    logging.info('Step 1: Getting PubMed publication mentions of each drug.')
    job1_status = step1_job(STEP1_CONFIGS)
    check_job_status(job1_status)

    logging.info('Step 2: Getting scientific publication mentions of each drug.')
    job2_status = step2_job(STEP2_CONFIGS, STEP1_CONFIGS.get('STEP1_OUTPUT_PATH'))
    check_job_status(job2_status)

    logging.info(f'Data pipeline finished with success and the result is in the file: '
                 f'"{STEP2_CONFIGS["STEP2_OUTPUT_PATH"]}".')
