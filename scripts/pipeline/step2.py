"""
Data pipeline's step2 entry point
"""
import logging

from scripts.common.utils import data_list_to_json_file, read_json_file
from scripts.pipeline.data_tools import add_mention_to_drug_if_found, read_input_file, archive_file, run_ingestion_job


def _find_drugs_scientific_publication_mentions(step2_configs, drugs_with_pubmed_mentions_file):
    """
    Find scientific publication mentions for each drug
    :param step2_configs: The configs of the step2
    :param drugs_with_pubmed_mentions_file: The output file path of step1
    """
    logging.info(f'Reading output file: "{drugs_with_pubmed_mentions_file}" of the previous step')
    drugs = read_json_file(drugs_with_pubmed_mentions_file)

    logging.info('Reading clinical trials file to get publication and journal mentions.')
    mentions, tmp_file_path = read_input_file(step2_configs.get('CLINICAL_TRIALS_FILE_PATH'))
    logging.info(f'Found {len(mentions)} scientific publication mentions.')

    logging.info('Finding drugs scientific publication mentions...')
    for drug in drugs:
        drug['scientific_mentions'] = []
        for mention in mentions:
            add_mention_to_drug_if_found(drug, mention, step2_configs.get('CLINICAL_COLUMN_MENTION_NAME'),
                                         'scientific_mentions', 'journal_mentions')

    logging.info('Writing result in output file...')
    data_list_to_json_file(drugs, step2_configs.get('STEP2_OUTPUT_PATH'))

    archive_file(tmp_file_path)


def step2_job(step2_configs: dict, drugs_with_pubmed_mentions_file: str) -> str:
    """
    Run step2 job
    :param step2_configs: The configs of the step2
    :param drugs_with_pubmed_mentions_file: The output file path of step1
    :return: The status of the job
    """
    return run_ingestion_job('step2_job', _find_drugs_scientific_publication_mentions, step2_configs,
                             drugs_with_pubmed_mentions_file)
