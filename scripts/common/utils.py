"""
Project utils and shared tools and functions
"""

import os
import logging
import re
import json
import csv


def read_csv_file(file_path: str) -> list:
    """
    Read CSV file from file
    :param file_path: The path of CSV file
    :return: A list of dictionary, each dictionary present a row in the CSV file
    """
    logging.info(f'Reading CSV file: "{file_path}"')
    with open(file_path, 'r', encoding='utf-8') as csv_file:
        data = list(csv.DictReader(csv_file))
    return data


def data_list_to_json_file(data_list: list, file_path: str):
    """
    Write a list of data(dictionary) in a json file
    :param data_list: The list of data
    :param file_path: The path of the JSON file in which the list will be written
    """
    with open(file_path, 'w', encoding='utf-8') as output_file:
        json.dump(data_list, output_file, indent=4)


def read_json_file(file_path: str) -> list:
    """
    Read a JSON file
    :param file_path: The path of CSV file
    :return: A list of dictionary, each dictionary present a JSON object in the JSON file
    """
    with open(file_path, 'r', encoding='utf-8') as file:
        return json.load(file)


def get_file_name_from_path(file_path: str) -> str:
    """
    Get the name of the file in a path
    :param file_path: The path of the file
    :return: The filename if exists
    """
    file_name = re.findall(r'.*\/(.*\..*)', file_path)
    if len(file_name) != 1:
        raise Exception(f'Source file path: "{file_path}" is not correct.')
    return file_name[0]


def move_file(source_path: str, destination_path: str) -> str:
    """
    Move a file from source path to destination path
    :param source_path: The path of the source folder with the name of the file
    :param destination_path: THe path of the destination path without the name of the file
    :return: The new path of the file in the destination path
    """
    logging.info(f'Moving file from: "{source_path}" to: "{destination_path}".')
    file_name = get_file_name_from_path(source_path)

    new_path = f'{destination_path}/{file_name}'
    os.rename(source_path, new_path)
    logging.info('File is moved with success.')
    return new_path
