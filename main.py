from scripts.pipeline.data_pipeline import run_data_pipeline
from scripts.analyze.journal_with_most_mentions import get_journal_with_most_mentions

if __name__ == '__main__':
    run_data_pipeline()
    get_journal_with_most_mentions()
